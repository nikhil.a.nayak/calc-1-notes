This math is inline $`a^2+b^2=c^2`$.

This is on a separate line

```math
\lim_{h\to0} (f(x+h)+f(x))/h
```
